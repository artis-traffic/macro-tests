/**
 * @file tests/traffic/macro/one_link.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MACRO_TESTS_ONE_LINK_HPP
#define ARTIS_TRAFFIC_MACRO_TESTS_ONE_LINK_HPP

#include <artis-star/kernel/dtss/GraphManager.hpp>

#include <artis-traffic/macro/core/Link.hpp>
#include <artis-traffic/macro/utils/End.hpp>
#include <artis-traffic/macro/utils/Generator.hpp>

/************************************************************
 *  Generator -> Link -> End
 ************************************************************/

template<typename FlowFunction>
struct OnlyOneLinkParameters : public artis::dtss::Parameters<artis::common::DoubleTime> {
  artis::traffic::macro::utils::GeneratorParameters<FlowFunction> generator_parameters;
  artis::traffic::macro::core::LinkParameters link_parameters;
  artis::traffic::macro::utils::EndParameters end_parameters;
};

template<typename FlowFunction>
class OnlyOneLinkGraphManager :
  public artis::dtss::GraphManager<artis::common::DoubleTime, OnlyOneLinkParameters<FlowFunction>,
    artis::common::NoParameters> {
public:
  enum sub_models {
    GENERATOR, LINK, END
  };

  OnlyOneLinkGraphManager(artis::common::Coordinator<artis::common::DoubleTime> *coordinator,
                          const OnlyOneLinkParameters<FlowFunction> &parameters,
                          const artis::common::NoParameters &graph_parameters)
    :
    artis::dtss::GraphManager<artis::common::DoubleTime, OnlyOneLinkParameters<FlowFunction>, artis::common::NoParameters>(
      coordinator, parameters, graph_parameters),
    _generator("generator", parameters.generator_parameters, parameters.time_step),
    _link("link", parameters.link_parameters, parameters.time_step),
    _end("end", parameters.end_parameters, parameters.time_step) {
    this->add_child(GENERATOR, &_generator);
    this->add_child(LINK, &_link);
    this->add_child(END, &_end);

    this->out({&_generator, artis::traffic::macro::utils::Generator<FlowFunction>::outputs::OUT})
      >> this->in({&_link, artis::traffic::macro::core::Link::inputs::IN_DOWN});

    this->out({&_link, artis::traffic::macro::core::Link::outputs::OUT_CLOSE})
      >> this->in({&_generator, artis::traffic::macro::utils::Generator<FlowFunction>::inputs::IN_CLOSE});
    this->out({&_link, artis::traffic::macro::core::Link::outputs::OUT_OPEN})
      >> this->in({&_generator, artis::traffic::macro::utils::Generator<FlowFunction>::inputs::IN_OPEN});

    this->out({&_link, artis::traffic::macro::core::Link::outputs::OUT_DOWN})
      >> this->in({&_end, artis::traffic::macro::utils::End::inputs::IN_DOWN});
    this->out({&_end, artis::traffic::macro::utils::End::outputs::OUT_UP})
      >> this->in({&_link, artis::traffic::macro::core::Link::inputs::IN_UP});
  }

  ~OnlyOneLinkGraphManager() override = default;

private:
  artis::dtss::Simulator<artis::common::DoubleTime, artis::traffic::macro::utils::Generator<FlowFunction>, artis::traffic::macro::utils::GeneratorParameters<FlowFunction >> _generator;
  artis::dtss::Simulator<artis::common::DoubleTime, artis::traffic::macro::core::Link, artis::traffic::macro::core::LinkParameters> _link;
  artis::dtss::Simulator<artis::common::DoubleTime, artis::traffic::macro::utils::End, artis::traffic::macro::utils::EndParameters> _end;
};

#endif //ARTIS_TRAFFIC_MACRO_TESTS_ONE_LINK_HPP
