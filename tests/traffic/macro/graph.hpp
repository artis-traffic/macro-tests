/**
 * @file tests/traffic/macro/graph.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MACRO_TESTS_GRAPH_HPP
#define ARTIS_TRAFFIC_MACRO_TESTS_GRAPH_HPP

#include <artis-star/kernel/dtss/GraphManager.hpp>

#include <artis-traffic/macro/core/Link.hpp>
#include <artis-traffic/macro/utils/End.hpp>
#include <artis-traffic/macro/utils/Generator.hpp>

struct graph_flow_function {
  artis::traffic::macro::core::LinkData operator()(double /* t */) const {
    return {0, 0, 0.2, 0.2};
  }
};

struct GraphParameters : public artis::dtss::Parameters<artis::common::DoubleTime> {
  unsigned int column_number;
  unsigned int line_number;
};

class GraphGraphManager :
  public artis::dtss::GraphManager<artis::common::DoubleTime, GraphParameters, artis::common::NoParameters> {
public:
  enum sub_models {
    GENERATOR, END, LINK = 100, NODE = 200
  };

  GraphGraphManager(artis::common::Coordinator<artis::common::DoubleTime> *coordinator,
                    const GraphParameters &parameters,
                    const artis::common::NoParameters &graph_parameters)
    :
    artis::dtss::GraphManager<artis::common::DoubleTime, GraphParameters, artis::common::NoParameters>(
      coordinator, parameters, graph_parameters),
    _column_number(parameters.column_number),
    _line_number(parameters.line_number) {
    artis::traffic::macro::utils::GeneratorParameters<graph_flow_function> gp{5, 10., 8., 2., 74826, {}};

    for (unsigned int i = 0; i < _column_number; ++i) {
      std::stringstream ss;

      ss << "generator_" << (i + 1);
      gp.seed++;
      _generators.push_back(new Generator(ss.str(), gp, parameters.time_step));
      this->add_child(GENERATOR + i, _generators.back());
    }

    artis::traffic::macro::core::LinkParameters lp{500, 100, 27.77, 1.2, 10, 1, 0.1};
    for (unsigned int i = 0; i < _column_number * _line_number; ++i) {
      std::stringstream ss;

      ss << "link_" << (i + 1);
      _links.push_back(new Link(ss.str(), lp, parameters.time_step));
      this->add_child(LINK + i, _links.back());
    }

    artis::traffic::macro::utils::EndParameters ep{0.1};
    for (unsigned int i = 0; i < _column_number; ++i) {
      std::stringstream ss;

      ss << "end_" << (i + 1);
      _ends.push_back(new End(ss.str(), ep, parameters.time_step));
      this->add_child(END + i, _ends.back());
    }

    // generator <-> link
    for (unsigned int i = 0; i < _column_number; ++i) {
      this->out({_generators[i], artis::traffic::macro::utils::Generator<graph_flow_function>::outputs::OUT})
        >> this->in({_links[i * 10], artis::traffic::macro::core::Link::inputs::IN_DOWN});
    }

    // link <-> link
    for (unsigned int i = 0; i < _column_number; ++i) {
      for (unsigned int j = 0; j < _line_number - 1; ++j) {
        this->out({_links[i * _line_number + j], artis::traffic::macro::core::Link::outputs::OUT_DOWN})
          >> this->in({_links[i * _line_number + j + 1], artis::traffic::macro::core::Link::inputs::IN_DOWN});
        this->out({_links[i * _line_number + j + 1], artis::traffic::macro::core::Link::outputs::OUT_UP})
          >> this->in({_links[i * _line_number + j], artis::traffic::macro::core::Link::inputs::IN_UP});
      }
    }

    // link <-> end
    for (unsigned int i = 0; i < _column_number; ++i) {
      this->out({_links[i * _line_number + _line_number - 1], artis::traffic::macro::core::Link::outputs::OUT_DOWN})
        >> this->in({_ends[i], artis::traffic::macro::utils::End::inputs::IN_DOWN});
      this->out({_ends[i], artis::traffic::macro::utils::End::outputs::OUT_UP})
        >> this->in({_links[i * _line_number + _line_number - 1], artis::traffic::macro::core::Link::inputs::IN_UP});
    }
  }

  ~GraphGraphManager() override = default;

private:
  typedef artis::dtss::Simulator<artis::common::DoubleTime, artis::traffic::macro::utils::Generator<graph_flow_function>, artis::traffic::macro::utils::GeneratorParameters<graph_flow_function>> Generator;
  typedef artis::dtss::Simulator<artis::common::DoubleTime, artis::traffic::macro::core::Link, artis::traffic::macro::core::LinkParameters> Link;
  typedef artis::dtss::Simulator<artis::common::DoubleTime, artis::traffic::macro::utils::End, artis::traffic::macro::utils::EndParameters> End;

  unsigned int _column_number;
  unsigned int _line_number;

  std::vector<Generator *> _generators;
  std::vector<Link *> _links;
  std::vector<End *> _ends;
};

#endif //ARTIS_TRAFFIC_MACRO_TESTS_TWO_LINKS_HPP
