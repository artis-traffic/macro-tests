/**
 * @file tests/traffic/macro/links.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <artis-star/common/RootCoordinator.hpp>
#include <artis-star/common/observer/Output.hpp>
#include <artis-star/common/observer/TimedIterator.hpp>
#include <artis-star/kernel/dtss/Policy.hpp>

#include "four_links_and_one_node.hpp"
#include "graph.hpp"
#include "one_link.hpp"
#include "three_links_and_one_node.hpp"
#include "two_links.hpp"

#define BOOST_TEST_MODULE Links_Tests

#include <boost/test/unit_test.hpp>

#include <chrono>
#include <iostream>

using namespace std::chrono;
using namespace artis::traffic::macro::core;

/*************************************************
 * Tests
 *************************************************/

struct flow_function {
  double flow_max;

  LinkData operator()(double t) const {
    double flow;
    if (t == 0) {
      flow = 0;
    } else if (t <= 1800) {
      flow = flow_max * t / 1800;
    } else if (t <= 3600) {
      flow = flow_max;
    } else {
      flow = flow_max * (1 - (t - 3600) / 1800);
    }
    return {flow, 0, flow, flow};
  }
};

struct flow_function2 {
  double flow_max;

  LinkData operator()(double /* t */) const {
    return {flow_max, 0, flow_max, flow_max};
  }
};

// only one link
BOOST_AUTO_TEST_CASE(TestCase_1)
{
  OnlyOneLinkParameters<flow_function2> parameters = {1, {5, 10., 8., 2., 74826, flow_function2{0.64}},
                                                      {500, 100, 27.77, 1.2, 10, 1, 0.1},
                                                      {0.1}};
  artis::common::context::Context<artis::common::DoubleTime> context(0, 500);
  artis::common::RootCoordinator<
    artis::common::DoubleTime, artis::dtss::Coordinator<
      artis::common::DoubleTime,
      artis::dtss::LastBagPolicy,
      OnlyOneLinkGraphManager<flow_function2>,
      OnlyOneLinkParameters<flow_function2>>
  > rc(context, "root", parameters, artis::common::NoParameters());

  steady_clock::time_point t1 = steady_clock::now();

  rc.run(context);

  steady_clock::time_point t2 = steady_clock::now();

  duration<double> time_span = duration_cast<duration<double> >(t2 - t1);

  std::cout << "Duration: " << time_span.count() << std::endl;

  BOOST_CHECK(true);
}

// two links
BOOST_AUTO_TEST_CASE(TestCase_2)
{
  TwoNodesParameters<flow_function> parameters = {1, {5, 10., 8., 2., 74826, flow_function{0.64}},
                                                   {500, 100, 27.77, 1.2, 10, 1, 0.1},
                                                   {500, 100, 27.77, 1.2, 10, 1, 0.1},
                                                   {0.1}};
  artis::common::context::Context<artis::common::DoubleTime> context(0, 5400);
  artis::common::RootCoordinator<
    artis::common::DoubleTime, artis::dtss::Coordinator<
      artis::common::DoubleTime,
      artis::dtss::LastBagPolicy,
      TwoNodesGraphManager<flow_function>,
      TwoNodesParameters<flow_function>>
  > rc(context, "root", parameters, artis::common::NoParameters());

  steady_clock::time_point t1 = steady_clock::now();

  rc.run(context);

  steady_clock::time_point t2 = steady_clock::now();

  duration<double> time_span = duration_cast<duration<double> >(t2 - t1);

  std::cout << "Duration: " << time_span.count() << std::endl;

  BOOST_CHECK(true);
}

// three links and one node
BOOST_AUTO_TEST_CASE(TestCase_3)
{
  ThreeLinksAndOneNodeParameters<flow_function2> parameters = {1, {5, 10., 8., 2., 74826, flow_function2{0.64}},
                                                               {5, 10., 8., 2., 74827, flow_function2{0.64}},
                                                               {500, 100, 27.77, 1.2, 10, 1, 0.2},
                                                               {500, 100, 27.77, 1.2, 10, 1, 0.2},
                                                               {500, 100, 27.77, 1.2, 10, 1, 0.2},
                                                               {{0.25, 0.75}, {1}, {0.2}, {0.2}},
                                                               {0.2}};
  artis::common::context::Context<artis::common::DoubleTime> context(0, 1000);
  artis::common::RootCoordinator<
    artis::common::DoubleTime, artis::dtss::Coordinator<
      artis::common::DoubleTime,
      artis::dtss::LastBagPolicy,
      ThreeLinksAndOneNodeGraphManager<flow_function2>,
      ThreeLinksAndOneNodeParameters<flow_function2>>
  > rc(context, "root", parameters, artis::common::NoParameters());

  steady_clock::time_point t1 = steady_clock::now();

  rc.run(context);

  steady_clock::time_point t2 = steady_clock::now();

  duration<double> time_span = duration_cast<duration<double> >(t2 - t1);

  std::cout << "Duration: " << time_span.count() << std::endl;

  BOOST_CHECK(true);
}

// four links and one node
BOOST_AUTO_TEST_CASE(TestCase_4)
{
  FourLinksAndOneNodeParameters<flow_function> parameters = {1, {5, 10., 8., 2., 74826, flow_function{0.1}},
                                                              {5, 10., 8., 2., 74827, flow_function{0.64}},
                                                              {500, 100, 13, 1.2, 10, 1, 0.2},
                                                              {500, 100, 27.77, 1.2, 10, 1, 0.2},
                                                              {500, 100, 13, 1.2, 10, 1, 0.2},
                                                              {500, 100, 27.77, 1.2, 10, 1, 0.3},
                                                              {{0.25, 0.75}, {0.25, 0.75}, {0.2, 0.3}, {0.2, 0.3}},
                                                              {0.2}, {0.3}};
  artis::common::context::Context<artis::common::DoubleTime> context(0, 5400);
  artis::common::RootCoordinator<
    artis::common::DoubleTime, artis::dtss::Coordinator<
      artis::common::DoubleTime,
      artis::dtss::LastBagPolicy,
      FourLinksAndOneNodeGraphManager<flow_function>,
      FourLinksAndOneNodeParameters<flow_function>>
  > rc(context, "root", parameters, artis::common::NoParameters());

  steady_clock::time_point t1 = steady_clock::now();

  rc.run(context);

  steady_clock::time_point t2 = steady_clock::now();

  duration<double> time_span = duration_cast<duration<double> >(t2 - t1);

  std::cout << "Duration: " << time_span.count() << std::endl;

  BOOST_CHECK(true);
}

// graph
BOOST_AUTO_TEST_CASE(TestCase_5)
{
  GraphParameters parameters = {1, 10, 20};
  artis::common::context::Context<artis::common::DoubleTime> context(0, 3600 * 24);
  artis::common::RootCoordinator<
    artis::common::DoubleTime, artis::dtss::Coordinator<
      artis::common::DoubleTime,
      artis::dtss::LastBagPolicy,
      GraphGraphManager,
      GraphParameters>
  > rc(context, "root", parameters, artis::common::NoParameters());

  steady_clock::time_point t1 = steady_clock::now();

  rc.run(context);

  steady_clock::time_point t2 = steady_clock::now();

  duration<double> time_span = duration_cast<duration<double> >(t2 - t1);

  std::cout << "Duration: " << time_span.count() << std::endl;

  BOOST_CHECK(true);
}
