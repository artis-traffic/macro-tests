/**
 * @file tests/traffic/macro/three_links_and_one_node.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MACRO_TESTS_THREE_LINKS_AND_ONE_NODE_HPP
#define ARTIS_TRAFFIC_MACRO_TESTS_THREE_LINKS_AND_ONE_NODE_HPP

#include <artis-star/kernel/dtss/GraphManager.hpp>

#include <artis-traffic/macro/core/Link.hpp>
#include <artis-traffic/macro/core/Node.hpp>
#include <artis-traffic/macro/utils/End.hpp>
#include <artis-traffic/macro/utils/Generator.hpp>

/************************************************************
 *  Generator -> Link -|
 *                     |-> Node -> Link -> End
 *  Generator -> Link -|
 ************************************************************/

template <typename FlowFunction>
struct ThreeLinksAndOneNodeParameters : public artis::dtss::Parameters<artis::common::DoubleTime> {
  artis::traffic::macro::utils::GeneratorParameters<FlowFunction> generator_1_parameters;
  artis::traffic::macro::utils::GeneratorParameters<FlowFunction> generator_2_parameters;
  artis::traffic::macro::core::LinkParameters link_1_parameters;
  artis::traffic::macro::core::LinkParameters link_2_parameters;
  artis::traffic::macro::core::LinkParameters link_3_parameters;
  artis::traffic::macro::core::NodeParameters node_parameters;
  artis::traffic::macro::utils::EndParameters end_parameters;
};

template <typename FlowFunction>
class ThreeLinksAndOneNodeGraphManager :
  public artis::dtss::GraphManager<artis::common::DoubleTime, ThreeLinksAndOneNodeParameters<FlowFunction>,
    artis::common::NoParameters> {
public:
  enum sub_models {
    GENERATOR_1, GENERATOR_2, LINK_1, LINK_2, LINK_3, NODE, END
  };

  ThreeLinksAndOneNodeGraphManager(artis::common::Coordinator<artis::common::DoubleTime> *coordinator,
                                   const ThreeLinksAndOneNodeParameters<FlowFunction> &parameters,
                                   const artis::common::NoParameters &graph_parameters)
    :
    artis::dtss::GraphManager<artis::common::DoubleTime, ThreeLinksAndOneNodeParameters<FlowFunction>, artis::common::NoParameters>(
      coordinator, parameters, graph_parameters),
    _generator_1("generator_1", parameters.generator_1_parameters, parameters.time_step),
    _generator_2("generator_2", parameters.generator_2_parameters, parameters.time_step),
    _link_1("link_1", parameters.link_1_parameters, parameters.time_step),
    _link_2("link_2", parameters.link_2_parameters, parameters.time_step),
    _link_3("link_3", parameters.link_3_parameters, parameters.time_step),
    _node("node", parameters.node_parameters, parameters.time_step),
    _end("end", parameters.end_parameters, parameters.time_step) {
    this->add_child(GENERATOR_1, &_generator_1);
    this->add_child(GENERATOR_2, &_generator_2);
    this->add_child(LINK_1, &_link_1);
    this->add_child(LINK_2, &_link_2);
    this->add_child(LINK_3, &_link_3);
    this->add_child(NODE, &_node);
    this->add_child(END, &_end);

    // generator_1 <-> link_1
    this->out({&_generator_1, artis::traffic::macro::utils::Generator<FlowFunction>::outputs::OUT})
      >> this->in({&_link_1, artis::traffic::macro::core::Link::inputs::IN_DOWN});
    this->out({&_link_1, artis::traffic::macro::core::Link::outputs::OUT_CLOSE})
      >> this->in({&_generator_1, artis::traffic::macro::utils::Generator<FlowFunction>::inputs::IN_CLOSE});
    this->out({&_link_1, artis::traffic::macro::core::Link::outputs::OUT_OPEN})
      >> this->in({&_generator_1, artis::traffic::macro::utils::Generator<FlowFunction>::inputs::IN_OPEN});

    // generator_2 <-> link_2
    this->out({&_generator_2, artis::traffic::macro::utils::Generator<FlowFunction>::outputs::OUT})
      >> this->in({&_link_2, artis::traffic::macro::core::Link::inputs::IN_DOWN});
    this->out({&_link_2, artis::traffic::macro::core::Link::outputs::OUT_CLOSE})
      >> this->in({&_generator_2, artis::traffic::macro::utils::Generator<FlowFunction>::inputs::IN_CLOSE});
    this->out({&_link_2, artis::traffic::macro::core::Link::outputs::OUT_OPEN})
      >> this->in({&_generator_2, artis::traffic::macro::utils::Generator<FlowFunction>::inputs::IN_OPEN});

    // link_1 <-> node
    this->out({&_link_1, artis::traffic::macro::core::Link::outputs::OUT_DOWN})
      >> this->in({&_node, artis::traffic::macro::core::Node::inputs::IN_DOWN});
    this->out({&_node, artis::traffic::macro::core::Node::outputs::OUT_UP})
      >> this->in({&_link_1, artis::traffic::macro::core::Link::inputs::IN_UP});

    // link_2 <-> node
    this->out({&_link_2, artis::traffic::macro::core::Link::outputs::OUT_DOWN})
      >> this->in({&_node, artis::traffic::macro::core::Node::inputs::IN_DOWN + 1});
    this->out({&_node, artis::traffic::macro::core::Node::outputs::OUT_UP + 1})
      >> this->in({&_link_2, artis::traffic::macro::core::Link::inputs::IN_UP});

    // link_3 <-> node
    this->out({&_node, artis::traffic::macro::core::Node::outputs::OUT_DOWN})
      >> this->in({&_link_3, artis::traffic::macro::core::Link::inputs::IN_DOWN});
    this->out({&_link_3, artis::traffic::macro::core::Link::outputs::OUT_UP})
      >> this->in({&_node, artis::traffic::macro::core::Node::inputs::IN_UP});

    // link_3 <-> end
    this->out({&_link_3, artis::traffic::macro::core::Link::outputs::OUT_DOWN})
      >> this->in({&_end, artis::traffic::macro::utils::End::inputs::IN_DOWN});
    this->out({&_end, artis::traffic::macro::utils::End::outputs::OUT_UP})
      >> this->in({&_link_3, artis::traffic::macro::core::Link::inputs::IN_UP});
  }

  ~ThreeLinksAndOneNodeGraphManager() override = default;

private:
  artis::dtss::Simulator<artis::common::DoubleTime, artis::traffic::macro::utils::Generator<FlowFunction>, artis::traffic::macro::utils::GeneratorParameters<FlowFunction>> _generator_1;
  artis::dtss::Simulator<artis::common::DoubleTime, artis::traffic::macro::utils::Generator<FlowFunction>, artis::traffic::macro::utils::GeneratorParameters<FlowFunction>> _generator_2;
  artis::dtss::Simulator<artis::common::DoubleTime, artis::traffic::macro::core::Link, artis::traffic::macro::core::LinkParameters> _link_1;
  artis::dtss::Simulator<artis::common::DoubleTime, artis::traffic::macro::core::Link, artis::traffic::macro::core::LinkParameters> _link_2;
  artis::dtss::Simulator<artis::common::DoubleTime, artis::traffic::macro::core::Link, artis::traffic::macro::core::LinkParameters> _link_3;
  artis::dtss::Simulator<artis::common::DoubleTime, artis::traffic::macro::core::Node, artis::traffic::macro::core::NodeParameters> _node;
  artis::dtss::Simulator<artis::common::DoubleTime, artis::traffic::macro::utils::End, artis::traffic::macro::utils::EndParameters> _end;
};

#endif //ARTIS_TRAFFIC_MACRO_TESTS_THREE_LINKS_AND_ONE_NODE_HPP
